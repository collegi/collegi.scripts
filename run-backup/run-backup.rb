#
# This is just messing around in ruby. It should not be considered to be
# a good program.
#

require 'yaml'

class Directory
  attr_accessor :name
  attr_accessor :type
  attr_accessor :path

  def initialize(dir_name, dir_type, dir_path)
    @name=dir_name
    @type=dir_type
    @path=dir_path
  end

  def self.all
    ObjectSpace.each_object(self).to_a
  end

  def self.count
    all.count
  end
end

config = YAML.load_file('backup-config.yml')

config.each do |key, value|
  instance_variable_set("@#{key}", Directory.new(value["name"], value["type"], value["path"]))
end

puts Directory.all

ObjectSpace.each_object(Directory) do |dir|
  case dir.type
  when "root"
    puts dir.name + ", " + dir.type + ", " + dir.path
    test = File.directory?("#{dir.path}")
    puts test
  else
    puts dir.name + ", " + dir.type + ", " + @root.path + "/" + dir.path
    test = File.directory?("#{@root.path}" + "/" + "#{dir.path}")
    puts test
  end
end
